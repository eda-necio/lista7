#ifndef __disjunctDisjunctSet__h
#define __disjunctDisjunctSet__h

typedef struct TDisjunctSet *PTDisjunctSet, TDisjunctSet;
typedef struct TData *PTData, TData; 

struct TData
{
	int value;
	int root;
};

struct TDisjunctSet{
    PTData values; 
    int  length;
};

PTDisjunctSet disjunctSet_create(int size);
void disjunctSet_initialize(PTDisjunctSet ds, int numberOfElements);
int disjunctSet_find(PTDisjunctSet ds, int x);
void disjunctSet_makeSet (PTDisjunctSet ds);
void disjunctSet_union (PTDisjunctSet ds, int x, int y);
int  disjunctSet_sameSet(PTDisjunctSet ds, int x, int y);
void disjunctSet_print(PTDisjunctSet ds);
void disjunctSet_free(PTDisjunctSet ds);
#endif
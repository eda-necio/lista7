compile: disjunctSet.c main.c
	gcc -c disjunctSet.c main.c
	gcc -o disjunctSet disjunctSet.o main.o -lm

run:
	./disjunctSet

clear:
	rm *.o disjunctSet
#include "disjunctSet.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>



PTDisjunctSet disjunctSet_create(int size){
	PTDisjunctSet ds = (PTDisjunctSet) malloc (sizeof(PTDisjunctSet));
	if (ds != NULL){
		ds->length = size;
		ds->values = (PTData) malloc (sizeof(TData) * size);
		if (ds->values != NULL)
			return ds; 	
		return NULL;
	}
	return NULL;
	
}

void disjunctSet_initialize(PTDisjunctSet ds, int numberOfElements){

	for (int i = 0; i < ds->length; i++)
		ds->values[i].value = i;

	disjunctSet_makeSet(ds);

}

int disjunctSet_find(PTDisjunctSet ds, int x){
	for(int i =0; i < ds->length; i++)
			if(ds->values[i].value == x) 
				return ds->values[i].root;
			
		return -2;
}

void disjunctSet_makeSet (PTDisjunctSet ds){

	for(int i=0; i < ds->length; ++i)
		ds->values[i].root = ds->values[i].value; 			

}

void disjunctSet_union (PTDisjunctSet ds, int x, int y){

	int rootX = disjunctSet_find(ds,x);
		
	for(int i=0; i < ds->length; i++){
		if (ds->values[i].value == y){
			ds->values[i].root = rootX;
		}		
	}

}

int  disjunctSet_sameSet(PTDisjunctSet ds, int x, int y){

	return disjunctSet_find(ds, x) == disjunctSet_find(ds, y); 

}


void disjunctSet_print(PTDisjunctSet ds){
	printf("===========================================================\n");
	for(int i =0; i < ds->length; i++){
		printf("%d => %d\n", ds->values[i].value, ds->values[i].root);
	}
	printf("\n");
	printf("===========================================================\n");

}

void disjunctSet_free(PTDisjunctSet ds){

	free(ds->values);
	free(ds);
		
}

#include "disjunctSet.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[])
{	

	srand(time(NULL));

	int tam; //tamanho do set

	if (argc >= 1){
		tam = atoi(argv[1]);
	}
	else{		
		tam = rand() % 100;
	}

	printf("Criando um Disjunct Set com tamanho n ... %d\n", tam);
	PTDisjunctSet ds = disjunctSet_create(tam);


	printf("inicializando o DS... \n");
	disjunctSet_initialize(ds, tam);

	printf("Exibindo....\n");
	disjunctSet_print(ds);


	int a = rand() % tam, b = rand() % tam, qtde = rand() % tam; 
	printf("criando %d unioes...\n", qtde);
	for (int i = 0; i < qtde; i++){
		printf("promovendo a uniao de %d e %d\n", a, b);
		disjunctSet_union(ds, a, b);
		a = rand() % tam;
		b = rand() % tam;
	}

	printf("Exibindo....\n");
	disjunctSet_print(ds);


	qtde = rand() % tam;
	printf("Mostrando %d representantes...\n", qtde);
	for (int i = 0; i < qtde; i++){
		printf("Representante de %d eh %d\n", a, disjunctSet_find(ds, a));
		a = rand() % tam;
	}	

	qtde = rand() % tam;
	printf("testando %d vezes se dois elementos quaisquer fazem parte de um mesmo conjunto\n", qtde);
	for (int i = 0; i < qtde; i++){
		a = rand() % tam;
		b = rand() % tam; 
		printf("elementos %d e %d sao de um conjunto? ", a, b);
		if (disjunctSet_sameSet(ds, a, b))
			printf("Sim\n");
		else
			printf("Nao\n");
	}	

	/*

	int v = 65 + rand() % 26;
	printf("Tentando remover o elemento ...%c\n", v);
	set_remove(set, v);

	printf("Exibindo novamente....\n");
	set_print(set);


	printf("Visualizando a uniao entre dois conjuntos...\n");
	printf("Criando um Set2 com tamanho n ... %d\n", tam);
	PTDisjunctSet set2 = set_create(tam);

	printf("inicializando o set com %d elementos aleatorios\n", elem);
	set_initialize(set2, elem);

	printf("Exibindo....\n");
	set_print(set2);

	set2->bits = set_union(set, set2);
	printf("Exibindo uniao....\n");
	set_print(set2);

	printf("Visualizando a intersecao entre dois conjuntos...\n");
	printf("Criando um Set3 com tamanho n ... %d\n", tam);
	set2 = set_create(tam);

	printf("inicializando o conjunto com %d elementos aleatorios\n", elem);
	set_initialize(set2, elem);

	printf("Exibindo Set1....\n");
	set_print(set);
	printf("Exibindo Set2....\n");
	set_print(set2);

	set2->bits = set_intersection(set, set2);
	printf("Exibindo intersecao....\n");
	set_print(set2);


	printf("Visualizando a diferenca entre dois conjuntos...\n");
	printf("Criando um Set4 com tamanho n ... %d\n", tam);
	set2 = set_create(tam);

	printf("inicializando o conjunto com %d elementos aleatorios\n", elem);
	set_initialize(set2, elem);

	printf("Exibindo Set1....\n");
	set_print(set);
	printf("Exibindo Set4....\n");
	set_print(set2);

	set2->bits = set_difference(set, set2);
	printf("Exibindo a diferenca....\n");
	set_print(set2);

	printf("Visualizando subconjuntos entre dois conjuntos...\n");
	printf("Criando um Set5 com tamanho n/3 ... %d\n", tam/3);
	set2 = set_create(tam/3);

	printf("inicializando o conjunto com %d elementos aleatorios\n", elem);
	set_initialize(set2, elem);

	printf("Exibindo Set1....\n");
	set_print(set);
	printf("Exibindo Set5....\n");
	set_print(set2);

	if (set_checkSubSet(set, set2))
		printf("Sao subconjuntos\n");
	else
		printf("Nao sao subconjuntos\n");


	printf("Visualizando se dois conjuntos sao iguais...\n");
	printf("Criando um Set6 com tamanho n ... %d\n", tam);
	set2 = set_create(tam);

	printf("inicializando o conjunto com %d elementos aleatorios\n", elem);
	set_initialize(set2, elem);

	printf("Exibindo Set1....\n");
	set_print(set);
	printf("Exibindo Set6....\n");
	set_print(set2);

	if (set_checkEquals(set, set2))
		printf("Sao iguais\n");
	else
		printf("Nao sao iguais\n");

	printf("Visualizando o complemento do Set6...\n");
	set2->bits = set_complement(set2);
	set_print(set2);


	printf("Visualizando o total de elemento de um conjunto...\n");
	printf("Criando um Set7 com tamanho n ... %d\n", tam);
	set2 = set_create(tam);

	printf("inicializando o conjunto com %d elementos aleatorios\n", elem);
	set_initialize(set2, elem);

	printf("Exibindo....\n");
	set_print(set2);
	printf("Total de elementos: %d\n", set_elementsCount(set2));


	printf("Visualizando a consulta de um elemento se eh pertencente ao conjunto...\n");

	v = 65 + rand() % 26;
	printf("Elemento a ser consultado: %c\n", v);

	if (set_checkElement(set2, v))
		printf("%c pertence a Set7\n", v);
	else
		printf("%c NAO pertence a Set7\n", v);

*/

	printf("Liberando o conjunto disjunto ....\n");
	disjunctSet_free(ds);

	return 0;
}